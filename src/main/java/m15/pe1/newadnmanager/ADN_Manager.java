/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pe1.newadnmanager;

/**
 *
 * @author tarda
 */
public class ADN_Manager {

    /**
     * Funcio que agafa l'atribut ADN i el retorna invertit
     *
     * @return ADN invertit.
     */
    public String invertADN(String ADN) {
        StringBuilder builder = new StringBuilder(ADN);
        return builder.reverse().toString();
    }

    
    /**
     * 
     * Count all adenine and return the amount
     * 
     * @return all the adenine that are added throughout the chain
     */
    public int numAdenines(String ADN) {
        int a = 0;
        //char[] letter = this.adn.toUpperCase().toCharArray();
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'A') {
                a++;
            }
        }
        return a;
    }

    /**
    *
    * Count all guanine and return the amount
    * 
    * @return all the guanine that is added along the chain
    */
    public int numGuanines(String ADN) {
        int g = 0;
        //char[] letter = this.adn.toUpperCase().toCharArray();
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'G') {
                g++;
            }
        }
        return g;
    }

    /**
     * Count all thymine and return the amount
     *
     * @return all the thymine that is added along the chain
     */
    public int numTimines(String ADN) {
        int t = 0;
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'T') {
                t++;
            }
        }
        return t;
    }

    /**
     * Count all cytokine and return the amount
     *
     * @return all the cytokine that is added along the chain
     */
    public int numCitosines(String ADN) {
        int c = 0;
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'C') {
                c++;
            }
        }
        return c;
    }

    /**
     * This function compares the count of the letters
     *
     * @return  The letter that has more count than the rest
     */
    public String maxLetter(String ADN) {
        int max = 0;
        String base;
        int a = numAdenines(ADN);
        int c = numCitosines(ADN);
        int g = numGuanines(ADN);
        int t = numTimines(ADN);

        if (a > c && a > g && a > t) {
            base = "A";
            max = a;
        } else if (c > a && c > g && c > t) {
            base = "C";
            max = c;
        } else if (g > a && g > c && g > t) {
            base = "G";
            max = g;
        } else {
            base = "T";
            max = t;
        }

        return base;
    }
 /**
     * Funcio que compara el recompte de totes les lletres
     *
     * @return La lletra que té menys recompte que la resta
     * 
     */
    public String minLetter(String ADN) {
        int min = 0;
        String base;
        int a = numAdenines(ADN);
        int c = numCitosines(ADN);
        int g = numGuanines(ADN);
        int t = numTimines(ADN);

        if (a < c && a < g && a < t) {
            base = "A";
            min = a;
        } else if (c < a && c < g && c < t) {
            base = "C";
            min = c;
        } else if (g < a && g < c && g < t) {
            base = "G";
            min = g;
        } else {
            base = "T";
            min = t;
        }

        return base;
    }

    
}
